package com.example.finalExamApi.domain.repositories;

import com.example.finalExamApi.domain.application.models.Language;
import com.example.finalExamApi.domain.application.models.Question;
import com.example.finalExamApi.domain.application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IQuestionRepository extends CrudRepository<Question, String> {
    List<Question> findByTitleContainingIgnoreCase(@Param("title") String title);
    List<Question> findByLanguage( Language language);
    List<Question> findByAuthor( User user);
}
