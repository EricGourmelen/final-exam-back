package com.example.finalExamApi.domain.repositories;

import com.example.finalExamApi.domain.application.models.Response;
import com.example.finalExamApi.domain.application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {
    User findByUsername(String username);
}
