package com.example.finalExamApi.domain.repositories;

import com.example.finalExamApi.domain.application.models.Question;
import com.example.finalExamApi.domain.application.models.Response;
import com.example.finalExamApi.domain.application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IResponseRepository extends CrudRepository<Response, String> {
    List<Response> findByAuthor(User user);
}
