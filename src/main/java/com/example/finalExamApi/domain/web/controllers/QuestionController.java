package com.example.finalExamApi.domain.web.controllers;

import com.example.finalExamApi.domain.application.models.Question;
import com.example.finalExamApi.domain.application.models.Response;
import com.example.finalExamApi.domain.application.services.QuestionService;
import com.example.finalExamApi.domain.application.services.ResponseService;
import com.example.finalExamApi.domain.web.dtos.QuestionDto;
import com.example.finalExamApi.domain.web.dtos.QuestionDtoFull;
import com.example.finalExamApi.domain.web.dtos.ResponseDto;
import com.example.finalExamApi.domain.web.dtos.ResponseDtoFull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/questions")
public class QuestionController {
    QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/{tid}")
    public ResponseEntity<QuestionDtoFull> getQuestion(@PathVariable String tid) {
        return new ResponseEntity<>( this.questionService.mapperQuestionDtoFull(this.questionService.getQuestion(tid).get()), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<QuestionDtoFull>> getQuestions(@RequestParam Optional<String> title,@RequestParam Optional<String> language, @RequestParam Optional<String> noResponse) {
        if(title.isPresent()){
            return new ResponseEntity( this.questionService.getQuestionByTitle(title.get()).stream().map( r -> this.questionService.mapperQuestionDtoFull(r)), HttpStatus.OK);
        }
        if(language.isPresent()){
            return new ResponseEntity( this.questionService.getQuestionByLanguage(language.get()).stream().map( r -> this.questionService.mapperQuestionDtoFull(r)), HttpStatus.OK);
        }
        if(noResponse.isPresent()){
            return new ResponseEntity( this.questionService.getQuestionsNoResponse().stream().map( r -> this.questionService.mapperQuestionDtoFull(r)), HttpStatus.OK);
        }
        return new ResponseEntity( this.questionService.getQuestions().stream().map( r -> this.questionService.mapperQuestionDtoFull(r)), HttpStatus.OK);
    }

    @PostMapping()
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity addQuestion(@RequestBody QuestionDto questionDto){
        ResponseEntity result = new ResponseEntity<Response>(HttpStatus.BAD_REQUEST);
        result = new ResponseEntity<>(this.questionService.addQuestion(questionDto), HttpStatus.CREATED);
        return result;
    }

    @PatchMapping("/{tid}")
    public ResponseEntity reportQuestion(@PathVariable String tid){
        if (this.questionService.reportQuestion(tid))
            return new ResponseEntity(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{tid}")
    public ResponseEntity deleteQuestion(@PathVariable String tid){
        if (this.questionService.deleteQuestion(tid))
            return new ResponseEntity(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
