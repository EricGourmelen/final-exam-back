package com.example.finalExamApi.domain.web.controllers;
import com.example.finalExamApi.domain.application.models.User;
import com.example.finalExamApi.domain.application.services.QuestionService;
import com.example.finalExamApi.domain.application.services.ResponseService;
import com.example.finalExamApi.domain.application.services.UserService;
import com.example.finalExamApi.domain.web.dtos.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")

public class UserController {

    private final UserService userService;
    private final QuestionService questionService;
    private final ResponseService responseService;

    public UserController(UserService userService, QuestionService questionService, ResponseService responseService) {
        this.userService = userService;
        this.questionService = questionService;
        this.responseService = responseService;
    }

    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
        return
                SecurityContextHolder.getContext().getAuthentication().getName();
    }
    @PostMapping()
    @ResponseStatus(value = HttpStatus.CREATED)
    public CreatedUserDto createUser(@RequestBody CreateUserDto createUserDto) {
        User user =
                this.userService.createUser(createUserDto.getPseudo(),createUserDto.getMail(),
                        createUserDto.getPassword());
        return new CreatedUserDto(user.getTid(), user.getUsername(), user.getEmail());
    }

    @GetMapping("/{name}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<QRUserDto> getQuestionResponseByUser(@PathVariable String name) {
        List<QuestionDtoFull> questions = this.questionService.getQuestionsByAuthor(name).stream().map(q -> this.questionService.mapperQuestionDtoFull(q)).toList();
        List<ResponseDtoFull> responses = this.responseService.getResponsesByAuthor(name).stream().map(r -> this.responseService.mapperResponseDtoFull(r)).toList();
        for (QuestionDtoFull question: questions) {
            question.responses = null;
        }
        QRUserDto result = new QRUserDto(questions,responses);
        return new ResponseEntity( result, HttpStatus.OK);
    }

}
