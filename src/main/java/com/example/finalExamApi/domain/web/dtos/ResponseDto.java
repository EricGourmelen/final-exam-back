package com.example.finalExamApi.domain.web.dtos;

import java.text.SimpleDateFormat;

public class ResponseDto {
    public String content;
    public String questionTid;

    public String getContent() {
        return content;
    }

    public String getQuestionTid() {
        return questionTid;
    }
}
