package com.example.finalExamApi.domain.web.dtos;

import com.example.finalExamApi.domain.application.models.Question;
import com.example.finalExamApi.domain.application.models.User;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

public class ResponseDtoFull {
    public String tid;
    public String content;
    public Timestamp creationDate;
    public String questionTid;
    public String author;
    public String authorEmail;
    public boolean isReported;
    public String voters;

    public ResponseDtoFull(String tid, String content, Timestamp creationDate, String questionTid, String author, String authorEmail, boolean isReported, String voters) {
        this.tid = tid;
        this.content = content;
        this.creationDate = creationDate;
        this.questionTid = questionTid;
        this.author = author;
        this.authorEmail = authorEmail;
        this.isReported = isReported;
        this.voters = voters;
    }

    public String getTid() {
        return tid;
    }

    public String getContent() {
        return content;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public String getQuestionTid() {
        return questionTid;
    }

    public String getAuthor() {
        return author;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public boolean isReported() {
        return isReported;
    }

    public String getVoters() {
        return voters;
    }
}
