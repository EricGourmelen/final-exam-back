package com.example.finalExamApi.domain.web.dtos;

import java.util.List;

public class QRUserDto {
    public List<QuestionDtoFull> questions;
    public List<ResponseDtoFull> responses;

    public QRUserDto(List<QuestionDtoFull> questions, List<ResponseDtoFull> responses) {

        this.questions = questions;
        this.responses = responses;
    }

    public List<QuestionDtoFull> getQuestions() {
        return questions;
    }

    public List<ResponseDtoFull> getResponses() {
        return responses;
    }
}
