package com.example.finalExamApi.domain.web.controllers;

import com.example.finalExamApi.domain.application.models.Response;
import com.example.finalExamApi.domain.application.services.ResponseService;
import com.example.finalExamApi.domain.web.dtos.ResponseDto;
import com.example.finalExamApi.domain.web.dtos.ResponseDtoFull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/responses")
public class ResponseController {
    ResponseService responseService;

    public ResponseController(ResponseService responseService) {
        this.responseService = responseService;
    }

    @GetMapping("/{tid}")
    public ResponseEntity<ResponseDtoFull> getResponse(@PathVariable String tid) {
        return new ResponseEntity<>( this.responseService.mapperResponseDtoFull(this.responseService.getResponse(tid).get()), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<Response>> getReponses() {
        return new ResponseEntity( this.responseService.getResponses().stream().map( r -> this.responseService.mapperResponseDtoFull(r)), HttpStatus.OK);
    }

    @PostMapping()
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity addResponse(@RequestBody ResponseDto responseDto){
        ResponseEntity result = new ResponseEntity<Response>(HttpStatus.BAD_REQUEST);
        result = new ResponseEntity<>(this.responseService.addResponse(responseDto), HttpStatus.CREATED);
        return result;
    }

    @PatchMapping("/report/{tid}")
    public ResponseEntity reportResponse(@PathVariable String tid){
        if (this.responseService.reportResponse(tid))
            return new ResponseEntity(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PatchMapping("/vote/{tid}")
    public ResponseEntity voteResponse(@PathVariable String tid){
        this.responseService.addVoter(tid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/unvote/{tid}")
    public ResponseEntity unVoteResponse(@PathVariable String tid){
        this.responseService.unVote(tid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{tid}")
    public ResponseEntity deleteResponse(@PathVariable String tid){
        if (this.responseService.deleteResponse(tid))
            return new ResponseEntity(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
