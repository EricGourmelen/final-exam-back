package com.example.finalExamApi.domain.web.dtos;

public class QuestionDto {
    public String content;
    public String title;
    public String language;

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getLanguage() {
        return language;
    }
}
