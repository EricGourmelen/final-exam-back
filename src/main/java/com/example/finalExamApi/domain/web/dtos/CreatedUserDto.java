package com.example.finalExamApi.domain.web.dtos;

public class CreatedUserDto {
    public String tid;
    public String username;
    public String email;

    public CreatedUserDto(String tid, String username, String email) {
        this.tid = tid;
        this.username = username;
        this.email = email;
    }

    public String getTid() {
        return tid;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }
}
