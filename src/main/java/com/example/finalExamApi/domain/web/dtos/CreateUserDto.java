package com.example.finalExamApi.domain.web.dtos;

public class CreateUserDto {
    public String pseudo;
    public String password;
    public String mail;

    public String getPseudo() {
        return pseudo;
    }

    public String getPassword() {
        return password;
    }

    public String getMail() {
        return mail;
    }
}
