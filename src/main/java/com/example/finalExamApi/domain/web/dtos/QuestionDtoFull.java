package com.example.finalExamApi.domain.web.dtos;

import com.example.finalExamApi.domain.application.models.Language;
import com.example.finalExamApi.domain.application.models.Response;

import java.sql.Timestamp;
import java.util.List;

public class QuestionDtoFull {
    public String tid;
    public String content;
    public String title;
    public Timestamp creationDate;
    public String author;
    public String authorEmail;
    public boolean isReported;
    public List<ResponseDtoFull> responses;
    public Language language;

    public QuestionDtoFull(String tid, String content, String title, Timestamp creationDate, String author, String authorEmail, boolean isReported, List<ResponseDtoFull> responses, Language language) {
        this.tid = tid;
        this.content = content;
        this.title = title;
        this.creationDate = creationDate;
        this.author = author;
        this.authorEmail = authorEmail;
        this.isReported = isReported;
        this.responses = responses;
        this.language = language;
    }

    public String getTid() {
        return tid;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }


    public String getAuthor() {
        return author;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public boolean isReported() {
        return isReported;
    }

    public List<ResponseDtoFull> getResponses() {
        return responses;
    }

    public Language getLanguage() {
        return language;
    }
}
