package com.example.finalExamApi.domain.application.services;

import com.example.finalExamApi.domain.application.models.Language;
import com.example.finalExamApi.domain.application.models.Question;
import com.example.finalExamApi.domain.application.models.Response;
import com.example.finalExamApi.domain.application.models.User;
import com.example.finalExamApi.domain.repositories.IQuestionRepository;
import com.example.finalExamApi.domain.web.dtos.QuestionDto;
import com.example.finalExamApi.domain.web.dtos.QuestionDtoFull;
import com.example.finalExamApi.domain.web.dtos.ResponseDtoFull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.NotAcceptableStatusException;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class QuestionService {

    IQuestionRepository questionRespository;
    UserService userService;
    ResponseService responseService;

    public QuestionService(IQuestionRepository questionRespository, UserService userService, ResponseService responseService) {
        this.responseService = responseService;
        this.questionRespository = questionRespository;
        this.userService = userService;
    }

    public List<Question> getQuestions(){
        List<Question> ordonateList = (List<Question>) this.questionRespository.findAll();
        return ordonateList.stream().sorted(Comparator.comparing(Question::getCreationDate)).collect(Collectors.toList());
    }

    public List<Question> getQuestionsNoResponse(){
        List<Question> questionList = (List<Question>) this.questionRespository.findAll();
        List<Question> questionWithoutResponse = new ArrayList<>();
        for (Question question : questionList) {
            if (question.getResponses().size() < 1){
                questionWithoutResponse.add(question);
            }
        }
        return questionWithoutResponse;
    }

    public Optional<Question> getQuestion(String tid){
        return this.questionRespository.findById(tid);
    }

    public boolean addQuestion(QuestionDto questionDto){
        boolean result = false;
        String[] checkTitle = questionDto.getTitle().split("\\s+");
        int checkQuestionMark = (checkTitle.length -1);
        if(checkTitle.length < 20 && checkTitle[checkQuestionMark].equals("?")){
            User user =
                    this.userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            Question question = new Question(UUID.randomUUID().toString(),questionDto.getContent(),questionDto.getTitle(), Language.valueOf(questionDto.getLanguage()));
            question.setAuthor(user);
            this.questionRespository.save(question);
            result = true;
        } else{
            throw new NotAcceptableStatusException("Le titre ne correspond pas aux critères");
        }
        return result;
    }

    public List<Question> getQuestionByTitle(String partialTitle){
        return this.questionRespository.findByTitleContainingIgnoreCase(partialTitle);
    }

    public List<Question> getQuestionByLanguage(String language){
        return this.questionRespository.findByLanguage(Language.valueOf(language));
    }

    public boolean reportQuestion(String tid){
        boolean result = true;
        Question questionToReport = this.getQuestion(tid).get();
        questionToReport.setReported(true);
        this.questionRespository.save(questionToReport);
        return result;
    }

    public boolean deleteQuestion(String tid) {
        boolean result = false;
        if (this.getQuestion(tid).get().isReported()) {
            this.questionRespository.deleteById(tid);
            result = true;
        }
        return result;
    }

    public QuestionDtoFull mapperQuestionDtoFull(Question question){
        return new QuestionDtoFull(
                question.getTid(),
                question.getContent(),
                question.getTitle(),
                question.getCreationDate(),
                question.getAuthor().getUsername(),
                question.getAuthor().getEmail(),
                question.isReported(),
                question.getResponses().stream().map( q -> this.responseService.mapperResponseDtoFull(q)).toList(),
                question.getLanguage()
                );
    }

    public List<Question> getQuestionsByAuthor(String name) {
        User user = this.userService.getUserByUsername(name);
        return this.questionRespository.findByAuthor(user);
    }
}
