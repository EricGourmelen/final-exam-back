package com.example.finalExamApi.domain.application.services;

import com.example.finalExamApi.domain.application.models.User;
import com.example.finalExamApi.domain.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    private final IUserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(IUserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User createUser(String username, String email, String
            password) {
        User newUser = new User(UUID.randomUUID().toString(),
                username, passwordEncoder.encode(password), email);
        userRepository.save(newUser);
        return newUser;
    }
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
