package com.example.finalExamApi.domain.application.services;

import com.example.finalExamApi.domain.application.models.Question;
import com.example.finalExamApi.domain.application.models.Response;
import com.example.finalExamApi.domain.application.models.User;
import com.example.finalExamApi.domain.repositories.IQuestionRepository;
import com.example.finalExamApi.domain.repositories.IResponseRepository;
import com.example.finalExamApi.domain.web.dtos.ResponseDto;
import com.example.finalExamApi.domain.web.dtos.ResponseDtoFull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.NotAcceptableStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ResponseService {

    IResponseRepository responseRepository;
    IQuestionRepository questionRepository;
    UserService userService;


    public ResponseService(IResponseRepository responseRepository, IQuestionRepository questionRepository, UserService userService) {
        this.responseRepository = responseRepository;
        this.questionRepository = questionRepository;
        this.userService = userService;
    }

    public List<Response> getResponses(){
        return (List<Response>) this.responseRepository.findAll();
    }

    public Optional<Response> getResponse(String tid){
        return this.responseRepository.findById(tid);
    }

    public boolean addResponse(ResponseDto responseDto){
        boolean result = false;
        String[] checkContent = responseDto.getContent().split("\\s+");
        if(checkContent.length < 100){
            User user =
                    this.userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            Response response = new Response(UUID.randomUUID().toString(),
                    responseDto.content, this.questionRepository.findById(responseDto.questionTid).get());
            response.setAuthor(user);
            response.setVoters(user.getTid());
            this.responseRepository.save(response);
            result = true;
        } else{
            throw new NotAcceptableStatusException("Le contenu ne doit pas dépasser les 100 mots");
        }
        return result;
    }

    public boolean reportResponse(String tid){
        boolean result = true;
        Response responseToReport = this.getResponse(tid).get();
        responseToReport.setReported(true);
        this.responseRepository.save(responseToReport);
        return result;
    }
    public boolean deleteResponse(String tid) {
        boolean result = false;
        if (this.getResponse(tid).get().isReported()) {
            this.responseRepository.deleteById(tid);
            result = true;
        }
        return result;
    }
    public void addVoter(String rTid){
        Response response = this.getResponse(rTid).get();
        User user = this.userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        String[] voters = response.getVoters().split("\\s+");
        boolean checkVoters = true;
        for (String voter: voters) {
            if (voter.equals(user.getTid())){
                checkVoters = false;
            }
        }
        if (checkVoters){
            response.addVoters(user.getTid());
            this.responseRepository.save(response);
        }
    }

    public void unVote(String rTid){
        Response response = this.getResponse(rTid).get();
        User user = this.userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        String[] voters = response.getVoters().split("\\s+");
        boolean checkVoters = false;
        for (String voter: voters) {
            if (voter.equals(user.getTid())){
                checkVoters = true;
            }
        }
        if (checkVoters){
            String toRemove = " " + user.getTid();
            response.setVoters(response.getVoters().replace(toRemove,""));
            this.responseRepository.save(response);
        }
    }

    public ResponseDtoFull mapperResponseDtoFull(Response response){
        return new ResponseDtoFull(response.getTid(), response.getContent(),response.getCreationDate(),response.getQuestion().getTid(),response.getAuthor().getUsername(),
                response.getAuthor().getEmail(),response.isReported(),response.getVoters());
    }

    public List<Response> getResponsesByAuthor(String name) {
        User user = this.userService.getUserByUsername(name);
        return this.responseRepository.findByAuthor(user);
    }

}
