package com.example.finalExamApi.domain.application.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
public class Question{
    @Id
    private String tid;
    private String content;
    private Timestamp creationDate;
    private String title;
    @JsonManagedReference
    @OneToMany(mappedBy = "question",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Response> responses;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_tid")
    private User author;
    @Enumerated(EnumType.STRING)
    @Column(unique = true, name = "language")
    private Language language;
    private boolean isReported;

    public Question() {
    }

    public Question(String tid, String content, String title, Language language) {
        this.tid = tid;
        this.content = content;
        this.title = title;
        this.creationDate = new Timestamp(new Date().getTime());
        this.language = language;
        this.isReported = false;
    }

    public String getContent() {
        return content;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public String getTitle() {
        return title;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public String getTid() {
        return tid;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @JsonIgnore
    public User getAuthor() {
        return author;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public boolean isReported() {
        return isReported;
    }

    public void setReported(boolean reported) {
        isReported = reported;
    }
}
