package com.example.finalExamApi.domain.application.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
public class Response{
    @Id
    private String tid;
    private String content;
    private Timestamp creationDate;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    private Question question;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_tid")
    private User author;
    private boolean isReported;
    private String voters;

    public Response() {
    }

    @Autowired
    public Response(String tid, String content, Question question) {
        this.tid = tid;
        this.content = content;
        this.question = question;
        this.creationDate = new Timestamp(new Date().getTime());
        this.isReported = false;
    }

    public String getContent() {
        return content;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public Question getQuestion() {
        return question;
    }

    public String getTid() {
        return tid;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getVoters() {
        return voters;
    }

    public void setVoters(String voters) {
        this.voters = voters;
    }

    public void addVoters(String voter) {
        this.voters =this.voters + " " + voter;
    }

    @JsonIgnore
    public User getAuthor() {
        return author;
    }

    public boolean isReported() {
        return isReported;
    }

    public void setReported(boolean reported) {
        isReported = reported;
    }
}
