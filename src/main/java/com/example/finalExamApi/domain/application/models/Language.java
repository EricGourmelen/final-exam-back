package com.example.finalExamApi.domain.application.models;

public enum Language {
    JAVA,
    PHP,
    JAVASCRIPT,
    TYPESCRIPT;
}
