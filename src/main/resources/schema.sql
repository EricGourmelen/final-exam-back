--drop table if exists response;
--drop table if exists question;
--drop table if exists users;


-- init database
create table IF NOT EXISTS users
(
    tid         char(36) primary key,
    username    text not null unique,
	email		text not null,
	password    text not null
);
create table IF NOT EXISTS question
(
    tid             char(36) primary key,
    title       	text not null,
    language       	text not null,
    content       	text not null,
	creation_date   TIMESTAMP WITH TIME ZONE NOT NULL,
	is_reported        boolean,
	users_tid    char(36) references users (tid)
);

create table IF NOT EXISTS response
(
    tid             char(36) primary key,
    content       	text not null,
	creation_date   TIMESTAMP WITH TIME ZONE NOT NULL,
	is_reported       boolean,
	voters            text,
    question_tid    char(36) references question (tid),
    users_tid    char(36) references users (tid)
);
